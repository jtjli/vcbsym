from django.shortcuts import render
from django.http import HttpResponse
from django.core.mail import mail_admins,send_mail
from django.conf import settings
import json
# Create your views here.
from django.views import View
from home.models import MailingList
class SubmitMailingListView(View):
    def post(self,request):
        s=request.POST["email"]
        try:
            MailingList.objects.create(email=s)
            return HttpResponse(json.dumps({"STATUS":"OK"}), content_type="application/json")
        except:
            return HttpResponse(json.dumps({"STATUS":"ERROR"}), content_type="application/json")


