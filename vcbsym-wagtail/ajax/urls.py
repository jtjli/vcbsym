from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from .views import SubmitMailingListView


urlpatterns = [
    url(r'^mailinglist$', SubmitMailingListView.as_view(), name='mailinglist'),
]
