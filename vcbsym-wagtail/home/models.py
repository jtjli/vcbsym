from django.db import models
from wagtail.core.models import Page
from wagtail.core.fields import RichTextField,StreamField
from wagtail.core import blocks
from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.images.blocks import ImageChooserBlock

class BioPage(Page):
    # speakers = StreamField([
    #     ('photo', ImageChooserBlock()),
    #     ('bio', blocks.RichTextBlock()),
    # ])
    # speakerBlock = ('speaker', blocks.StructBlock([
    #     ('photo', ImageChooserBlock()),
    #     ('bio', blocks.RichTextBlock()),
    # ]))
    speakers = StreamField([
        ('speaker', blocks.StructBlock([
            ('photo', ImageChooserBlock()),
            ('bio', blocks.RichTextBlock())
        ]))
    ])

    content_panels = Page.content_panels + [
        StreamFieldPanel('speakers'),
    ]

class MiscPage(Page):
    body=RichTextField(blank=True)
    body_section2=RichTextField(blank=True)
    content_panels = Page.content_panels + [
        FieldPanel('body', classname="full"),
        FieldPanel('body_section2', classname="full"),
    ]

class HomePage(Page):
    nav_brand=models.CharField(max_length=100, help_text="Text for Navigation Bar",null=True,blank=True)
    hero_title=models.CharField(max_length=100, help_text="Text for banner",null=True,blank=True)
    hero_image=models.ForeignKey('wagtailimages.Image',null=True,blank=True,on_delete=models.SET_NULL,related_name='+',
                                 help_text='Background Image for banner')
    hero_subtitle=models.CharField(max_length=200, help_text="Subtitle for banner",null=True,blank=True)
    slogan=models.CharField(max_length=200, help_text="Slogan below banner",null=True,blank=True)
    body=RichTextField(blank=True)
    body_section2=RichTextField(blank=True)
    body_section3=RichTextField(blank=True)
    speakers = StreamField([
        ('speaker', blocks.StructBlock([
            ('photo', ImageChooserBlock()),
            ('name', blocks.CharBlock(max_length=150)),
            ('link', blocks.URLBlock(required=False,max_length=200)),
        ]))
    ],null=True)

    sponsors = StreamField([
        ('sponsor', blocks.StructBlock([
            ('photo', ImageChooserBlock()),
            ('name', blocks.CharBlock(max_length=150)),
            ('link', blocks.URLBlock(required=False, max_length=200)),
        ]))
    ], null=True)

    footer=models.CharField(max_length=200, help_text="footer",null=True,blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('nav_brand', classname=""),
        FieldPanel('hero_title', classname="full title"),
        ImageChooserPanel('hero_image'),
        FieldPanel('hero_subtitle', classname="full"),
        FieldPanel('slogan', classname="full"),
        FieldPanel('body', classname="full"),
        FieldPanel('body_section3', classname="full"),
        StreamFieldPanel('speakers'),
        FieldPanel('body_section2', classname="full"),
        StreamFieldPanel('sponsors'),
        FieldPanel('footer', classname="full"),
    ]

class MailingList(models.Model):
    email = models.EmailField(max_length=100)