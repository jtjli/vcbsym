from wagtail.contrib.modeladmin.options import (
    ModelAdmin, modeladmin_register)
from .models import MailingList

class MailingListModelAdmin(ModelAdmin):
    model = MailingList
    menu_icon = 'date'
    menu_order= 200
    list_display = ('email',)


modeladmin_register(MailingListModelAdmin)