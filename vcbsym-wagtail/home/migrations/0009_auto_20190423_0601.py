# Generated by Django 2.1.7 on 2019-04-23 06:01

from django.db import migrations
import wagtail.core.blocks
import wagtail.core.fields
import wagtail.images.blocks


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0008_biopage_miscpage'),
    ]

    operations = [
        migrations.AlterField(
            model_name='biopage',
            name='speakers',
            field=wagtail.core.fields.StreamField([('speaker', wagtail.core.blocks.StructBlock([('photo', wagtail.images.blocks.ImageChooserBlock()), ('bio', wagtail.core.blocks.RichTextBlock())]))]),
        ),
    ]
