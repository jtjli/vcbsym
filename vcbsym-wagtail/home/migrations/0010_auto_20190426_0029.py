# Generated by Django 2.1.8 on 2019-04-26 00:29

from django.db import migrations
import wagtail.core.blocks
import wagtail.core.fields
import wagtail.images.blocks


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0009_auto_20190423_0601'),
    ]

    operations = [
        migrations.AddField(
            model_name='homepage',
            name='body_section3',
            field=wagtail.core.fields.RichTextField(blank=True),
        ),
        migrations.AddField(
            model_name='homepage',
            name='speakers',
            field=wagtail.core.fields.StreamField([('speaker', wagtail.core.blocks.StructBlock([('photo', wagtail.images.blocks.ImageChooserBlock()), ('name', wagtail.core.blocks.CharBlock(max_length=150))]))], null=True),
        ),
    ]
