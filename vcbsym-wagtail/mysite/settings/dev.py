from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'krek2y*tp1ljjvzqdg^5=suezyfyv#5yslc7$_4jrs$-5axq03'

# SECURITY WARNING: define the correct hosts in production!
ALLOWED_HOSTS = ['*'] 

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

DATABASES = {
    'default': {
    # 'coreweb-postgres-test': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'vcbsym',
        'USER': 'coreweb',
        'PASSWORD': 'coreweb123',
        'HOST': 'coreweb-postgres-test.cx6wgvflsls5.ap-southeast-2.rds.amazonaws.com',
        'PORT': '5432',

    },
}

try:
    from .local import *
except ImportError:
    pass
