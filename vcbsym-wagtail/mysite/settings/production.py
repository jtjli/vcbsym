from .base import *

DEBUG = False

SECRET_KEY = 'krek2y*tX982Hdg^5=suez0928slyslc7$_4jrs$-5axq03'
ALLOWED_HOSTS = ['cancerbioinformatics.au','viccancerbioinfsymposium.org']
DATABASES = {
    'default': {
    # 'coreweb-postgres-prod': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'vcbsym',
        'USER': 'coreweb',
        'PASSWORD': 'coreweb123',
        'HOST': 'coreweb-postgres-prod.cx6wgvflsls5.ap-southeast-2.rds.amazonaws.com',
        'PORT': '5432',

    },
}

try:
    from .local import *
except ImportError:
    pass
