wagtail-site/		The Django Wagtail site. Load this into PyCharm
./			Other files in the root directory are related to deployment


Development workflow:
- Code in PyCharm
- Test using PyCharm>Debug with remote debugging & dev Database
- When OK, git commit & push
- Login to AWS server (docker host), git pull, run build.sh then RUN_DEV.sh (to launch staging)
- Play with Staging site to make sure it's working
- When OK, use RUN_PROD.sh to update the production server




To adjust deployment environment,  modify docker-compose & nginx to suit local paths & ports.



===================================================================================================
== PyCharm Remote Debugging ( so you don't need to set up all dependencies on your local computer)
===================================================================================================
**Pulled from another project, please adapt accordingly:

0. Make sure you have git-cloned bitbucket codes to your local computer:
       git clone https://<user>@bitbucket.org/jtjli/<project>.git <reporoot>
   In PyCharm open <reporoot>/coreweb/  as a DJANGO project. Click YES to Create project from source when asked (some pycharm versions won't ask).
   (choose a random interpreter first; once project created, follow steps below.
     OR  create a remote interpreter directly using information below)
   (Under Preferences/File -> Project settings, check if Django is enabled with the right manage.py path )

1. If DEV containers are not already up and running, or if you want to launch an isolated development env:
    ssh to coreweb-dev  (13.238.21.31:22 , user:ubuntu, using ssh key coreweb-dev.ppk)
    cd /coreweb_src/dockercompose
    RUN_DEV.sh
    #XXX docker-compose -f docker-compose-amazon.yml up -d  
    #XXX docker exec dockercompose_core-web_1 service ssh start  # TO START SSH SERVER
    ### TODO: allow adding suffix to container & network names so we can launch multiple set of containers
    ###       using different ssh port mapping (to enable parallel development)

2. Make sure SSH is working for the container (try ssh in):  13.238.21.31:443 (or other ports specified above)
    Users:pwd: 
            lij:123
            niko:123
            franco:123
            
3. In PyCharm, Tools>Deployment>Configuration
      Click "+" , type a name (eg coreweb-dev-container) & choose SFTP
      Under "Connectoin": Fill in 
           host:13.238.21.31,port : 443, root path:"/" , username & password as above
      Under "Mappings": 
           Local path: <reporoot>/coreweb
           Deployment path:  /home/lij/coreweb   (/home/<user>/coreweb )
           Web path: /home/lij/coreweb
      (NOTE: /home/<user> are volumes mounted to the host, so it won't be destroyed with the containers)

4. In PyCharm, File>Settings>Project:..> Project Interpreter

    IF OLD PYCHARM:
          Click on the settings ICON near right-top  >  Add Remote
          Choose Deployment configuration > coreweb-dev-container
          Click "MOve" this server to IDE settings
          Python Interpreter path: /usr/bin/python3  (note the 3)
          Mapping/Sync folders:  change to <Project root> -->  /home/lij/coreweb
    IF NEW (2018) PYCHARM:
          Click on the settings ICON near right-top  > Add
          Choose SSH Interpreter > Existing server configuration
          Click "MOve" this server to IDE settings
          Click NEXT
          Interpreter: /usr/bin/python3  (note the 3)
          Sync folders:  change to <Project root> -->  /home/lij/coreweb

          (Some pycharm helper files will be uploaded in the background ... may take 10 mins?)
      
5.  INITIAL UPLOAD: Right click your project, click Upload to coreweb-dev-container

6.  PyCharm > Tools > Deployment > click Automatic Upload 
    All changes will be uploaded automatically in future.
    
6b. IF project was not opened with Django enabled, go to Settings > Languages & Frameworks > Django -- Enable it, check settings
    
7.  PyCharm > Run > Edit Configurations
        Under Django server > coreweb  (if does not exist, create a new django server )
        HOST: 0.0.0.0
        PORT: 8080   ### TODO: enable also 8081 8082  (for parallel development)
        Run Browser: http://13.238.21.31:8080/   (or change port)
        Make sure Environment Variables has DJANGO_SETTINGS_MODULE = coreweb.settings   and   PYTHONUNBUFFERED = 1
        Make sure Python interpreter is Remote Python 3.x.x

8.  File > Invalidate Caches & Restart      (to resolve  "unresolved references")
        
        