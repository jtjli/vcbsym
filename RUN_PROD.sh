#!/bin/bash

TEMP_PROD_YML=docker-compose-prod_tmp.yml

grep -v "PROD DELETE" docker-compose.yml | sed -e 's/settings.dev/settings.production/' | sed -e 's/${PROD_SUFFIX}/_PROD/' > $TEMP_PROD_YML

docker-compose -f $TEMP_PROD_YML -p prod up -d

rm $TEMP_PROD_YML
