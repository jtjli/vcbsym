from ubuntu:16.04

# Update packages
run apt-get -y upgrade
run apt-get update --fix-missing

# Install required packages
run apt-get install -y software-properties-common openssh-server
run add-apt-repository "deb http://cran.rstudio.com/bin/linux/ubuntu $(lsb_release -sc)/"
run apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E084DAB9
run apt-get update
run apt-get install -y python3 python3-dev python-software-properties libpcre3 libpcre3-dev sqlite3 gcc g++ vim rabbitmq-server nano wget r-base default-jdk xml2 libxml2-dev
#run apt-get install -qy python3-setuptools
run apt-get install -y python3-pip
run pip3 install  --upgrade pip setuptools
# Install pip
#run easy_install3 pip



# Add all files
#add . /home
COPY ./vcbsym-wagtail/requirements.txt /home/requirements.txt
RUN pip install --upgrade pip
# Install any needed packages specified in requirements.txt
RUN pip install -r /home/requirements.txt

COPY . /home/
WORKDIR /home/

# Adjust permissions
#run cd /home/coreweb && chmod 777 *sh

# Make static and media file directories
run mkdir -p /var/www/vcbsym/static/ 
run mkdir -p /var/www/vcbsym/media/

#Collect Static & Migrate
run python3 /home/vcbsym-wagtail/manage.py collectstatic --no-input
run python3 /home/vcbsym-wagtail/manage.py migrate --no-input

env TERM xterm

# add users for remote debugging  # ssh server to be started manually
run useradd -m -s /bin/bash lij && echo lij:123 | chpasswd
run useradd -m -s /bin/bash franco && echo franco:123 | chpasswd
run useradd -m -s /bin/bash niko && echo niko:123 | chpasswd
run useradd -m -s /bin/bash cassie && echo cassie:123 | chpasswd
run chown lij:lij /home/lij && chown niko:niko /home/niko && chown franco:franco /home/franco && chown cassie:cassie /home/cassie
