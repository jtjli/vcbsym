# VCBSYM - CMS for [VCBS Conference](http://viccancerbioinfsymposium.org)

This CMS is built on [Django Wagtail CMS](https://wagtail.org). At least two VCBS committee members with django knowledge are to support the maintenance of this code base. Maintenance of the server will be supported by Peter Mac Bioinformatics Core. Since this CMS is very user-friendly, any committee members will be able to moderate the content of this site.

## Directory structure

    wagtail-site/     The Django Wagtail site. Load this into PyCharm
    ./                Other files in the root directory are related to deployment


## Development workflow:
1. Modify codes in PyCharm
2. Test using PyCharm>Debug with remote debugging and dev Database 
3. When OK, git commit & push
4. Login to AWS server (docker host), git pull, run build.sh then RUN_DEV.sh (to launch staging)
5. Play with Staging site to make sure it's working
6. When OK, use RUN_PROD.sh to update the production server


To adjust deployment environment,  modify docker-compose & nginx to suit local paths & ports.

------------------------------

## PyCharm Remote Debugging (so you don't need to set up all dependencies on your local computer)

**STEP 1)** Make sure you have git-cloned bitbucket codes to your local computer:

    git clone git@bitbucket.org:jtjli/vcbsym.git 

* In PyCharm open <reporoot>/wagtail-site/  as a DJANGO project. Click YES to Create project from source when asked (some pycharm versions won't ask).
* Choose a random interpreter first; once project created, follow steps below, OR  create a remote interpreter directly using information below
* Under Preferences/File -> Project settings, check if Django is enabled with the right manage.py path

**STEP 2)** If DEV containers are not already up and running, or if you want to launch an isolated development env:

    ssh to _server_ user:ubuntu, using ssh key provided
    cd ~/vcbsym/dockercompose
    RUN_DEV.sh

**STEP 3)** Make sure SSH is working for the container (try ssh in):  _server_:443 (or other ports specified above)

    Users:pwd: (for illustrative purpose)
    lij:123
    niko:123
    franco:123
            
**STEP 4)** In PyCharm, Tools>Deployment>Configuration

* Click "+" , type a name (eg vcbsym-dev-container) & choose SFTP
* Under "Connectoin": Fill in: host:_server_,port : 443, root path:"/" , username & password as above
* Under "Mappings": 
    - Local path: <reporoot>/wagtail-site
    - Deployment path:  /home/lij/vcbsym   (/home/<user>/vcbsym )
    - Web path: /home/lij/vcbsym

NOTE: /home/<user> are volumes mounted to the host, so it won't be destroyed with the containers

**STEP 5)** In PyCharm, File>Settings>Project:..> Project Interpreter

* _FOR OLD PYCHARM:_
  Click on the settings ICON near right-top  >  Add Remote
  Choose Deployment configuration > vcbsym-dev-container
  Click "MOve" this server to IDE settings
  Python Interpreter path: /usr/bin/python3  (note the 3)
  Mapping/Sync folders:  change to <Project root> -->  /home/lij/vcbsym

* _FOR NEW (2018) PYCHARM:_
  Click on the settings ICON near right-top  > Add
  Choose SSH Interpreter > Existing server configuration
  Click "MOve" this server to IDE settings
  Click NEXT
  Interpreter: /usr/bin/python3  (note the 3)
  Sync folders:  change to <Project root> -->  /home/lij/vcbsym

  (Some pycharm helper files will be uploaded in the background ... may take 10 mins?)
      
**STEP 6)**  INITIAL UPLOAD: Right click your project, click Upload to vcbsym-dev-container

**STEP 7)**  PyCharm > Tools > Deployment > click Automatic Upload 
* All changes will be uploaded automatically in future.
* IF project was not opened with Django enabled, go to Settings > Languages & Frameworks > Django -- Enable it, check settings
    
**STEP 8)**  PyCharm > Run > Edit Configurations

* Under Django server > vcbsym  (if does not exist, create a new django server )
* HOST: 0.0.0.0
* PORT: 8080   ### TODO: enable also 8081 8082  (for parallel development)
* Run Browser: http://_server-ip_:8080/   (or change port)
* Make sure Environment Variables has DJANGO_SETTINGS_MODULE = vcbsym.settings   and   PYTHONUNBUFFERED = 1
* Make sure Python interpreter is Remote Python 3.x.x

**STEP 9)**  File > Invalidate Caches & Restart      (to resolve  "unresolved references")
        
        
